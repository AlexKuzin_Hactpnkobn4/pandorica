package com.alexkuzin.pandorica.security

import com.alexkuzin.pandorica.config.exceptionHandling.ExceptionHandlingFilter
import com.alexkuzin.pandorica.data.repositories.DynamoDBUsersRepo
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpStatus
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.core.userdetails.User
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.security.web.SecurityFilterChain
import org.springframework.security.web.authentication.HttpStatusEntryPoint
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter

@Configuration
@EnableWebSecurity
class SecurityConfig(
    val exceptionHandlingFilter: ExceptionHandlingFilter,
    val jwtRequestFilter: JwtRequestFilter,
) {

    @Bean
    fun filterChain(http: HttpSecurity): SecurityFilterChain {
        return http
            .csrf { csrf -> csrf.disable() }
            .cors{}
            .authorizeHttpRequests { httpRequest ->
                httpRequest
                    .requestMatchers("/auth/**").permitAll()
                    .anyRequest().authenticated()
            }
            .exceptionHandling{config ->
                config.authenticationEntryPoint(HttpStatusEntryPoint(HttpStatus.UNAUTHORIZED))
            }
            .addFilterBefore(exceptionHandlingFilter, UsernamePasswordAuthenticationFilter::class.java)
            .addFilterBefore(jwtRequestFilter, UsernamePasswordAuthenticationFilter::class.java)
            .sessionManagement { sessionConfig ->
                sessionConfig.sessionCreationPolicy(SessionCreationPolicy.STATELESS)
            }
            .build()
    }

    @Bean
    fun userDetailService(usersRepo: DynamoDBUsersRepo) =
        UserDetailsService { login ->
            val user = usersRepo.getUserByLogin(login)
            User.builder()
                .username(user.login)
                .password(user.encodedPassword)
                .build()
        }

    @Bean
    fun passwordEncoder(): PasswordEncoder = BCryptPasswordEncoder()

}