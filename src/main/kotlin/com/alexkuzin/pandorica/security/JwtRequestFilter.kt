package com.alexkuzin.pandorica.security

import com.alexkuzin.pandorica.utils.Constants
import jakarta.servlet.FilterChain
import jakarta.servlet.http.HttpServletRequest
import jakarta.servlet.http.HttpServletResponse
import org.springframework.http.HttpHeaders
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Component
import org.springframework.web.filter.OncePerRequestFilter

//TODO: Add access token revocation
@Component
class JwtRequestFilter(val jwtTokenService: JwtTokenService) : OncePerRequestFilter() {
    override fun doFilterInternal(
        request: HttpServletRequest,
        response: HttpServletResponse,
        filterChain: FilterChain
    ) {
        val authHeader = request.getHeader(HttpHeaders.AUTHORIZATION)
        if (authHeader == null || !authHeader.startsWith("Bearer ")) {
            filterChain.doFilter(request, response); return
        }

        val token = authHeader.substring(7)
        if (jwtTokenService.isExpired(token)) {
            filterChain.doFilter(request, response); return
        }

        val claims = jwtTokenService.getAllClaimsFromToken(token)

        val type = claims[Constants.Jwt.TOKEN_CLAIM_NAME_TYPE]
        if(type != Constants.Jwt.TOKEN_TYPE_ACCESS){
            filterChain.doFilter(request, response); return
        }

        val login = claims.subject

        val authentication = UsernamePasswordAuthenticationToken(login, null, emptyList())
        SecurityContextHolder.getContext().authentication = authentication

        filterChain.doFilter(request, response)
    }
}