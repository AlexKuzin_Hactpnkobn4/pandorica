package com.alexkuzin.pandorica.security

import com.alexkuzin.pandorica.config.SecurityProperties
import com.alexkuzin.pandorica.config.exceptionHandling.exceptions.AuthenticationException
import io.jsonwebtoken.Claims
import io.jsonwebtoken.ExpiredJwtException
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.security.Keys
import org.springframework.stereotype.Service
import java.time.Duration
import java.util.*


private const val TOKEN_TYPE_ACCESS = "ACCESS"
private const val TOKEN_TYPE_REFRESH = "REFRESH"
private const val TOKEN_CLAIM_NAME_TYPE = "type"
private const val TOKEN_CLAIM_NAME_FAMILY_ID = "familyId"

@Service
class JwtTokenService(
    val securityProperties: SecurityProperties
) {

    private val accessTokenDuration: Duration = Duration.parse(securityProperties.accessTokenDuration)
    private val refreshTokenDuration: Duration = Duration.parse(securityProperties.refreshTokenDuration)

    fun generateAccessToken(login: String, familyId: String): String {
        return generateToken(
            login,
            accessTokenDuration,
            familyId,
            TOKEN_TYPE_ACCESS
        )
    }

    fun generateRefreshToken(login: String, familyId: String): String {
        return generateToken(
            login,
            refreshTokenDuration,
            familyId,
            TOKEN_TYPE_REFRESH
        )
    }

    private fun generateToken(login: String, duration: Duration, familyId: String, type: String): String {
        val issuedDate = Date()
        val expireDate = Date(issuedDate.time + duration.toMillis())
        return Jwts.builder()
            .claims()
            .subject(login)
            .expiration(expireDate)
            .issuedAt(issuedDate)
            .add(TOKEN_CLAIM_NAME_TYPE, type)
            .add(TOKEN_CLAIM_NAME_FAMILY_ID, familyId)
            .and()
            .signWith(Keys.hmacShaKeyFor(securityProperties.secret.toByteArray()))
            .compact()
    }

    fun getAllClaimsFromToken(token: String): Claims {
        return try {
            Jwts.parser()
                .verifyWith(Keys.hmacShaKeyFor(securityProperties.secret.toByteArray())).build()
                .parseSignedClaims(token)
                .payload
        } catch (e: ExpiredJwtException) {
            return e.claims
        } catch (e: Exception) {
            throw AuthenticationException()
        }
    }

    fun isExpired(token: String): Boolean {
        try {
            Jwts.parser()
                .verifyWith(Keys.hmacShaKeyFor(securityProperties.secret.toByteArray())).build()
                .parseSignedClaims(token)
        } catch (e: ExpiredJwtException) {
            return true
        } catch (e: Exception) {
            throw AuthenticationException()
        }
        return false
    }
}