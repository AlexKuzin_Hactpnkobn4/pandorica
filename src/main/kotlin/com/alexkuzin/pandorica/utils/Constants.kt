package com.alexkuzin.pandorica.utils

class Constants {
    class DynamoDB {
        companion object {
            private const val prefix = "pandorica_"
            const val TABLE_NAME_USERS = prefix + "users"
            const val TABLE_NAME_VAULTS = prefix + "vaults"
            const val TABLE_NAME_TOKENS = prefix + "tokens"
        }
    }

    class Jwt{
        companion object{
            const val TOKEN_TYPE_ACCESS = "ACCESS"
            const val TOKEN_TYPE_REFRESH = "REFRESH"
            const val TOKEN_CLAIM_NAME_TYPE = "type"
            const val TOKEN_CLAIM_NAME_FAMILY_ID = "familyId"
        }
    }

}