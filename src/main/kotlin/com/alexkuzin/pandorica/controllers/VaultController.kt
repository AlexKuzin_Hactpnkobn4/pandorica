package com.alexkuzin.pandorica.controllers

import com.alexkuzin.pandorica.data.request.UpdateVaultRequest
import com.alexkuzin.pandorica.data.response.GetVaultResponse
import com.alexkuzin.pandorica.services.VaultService
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("vault")
class VaultController(val vaultService: VaultService) {

    @GetMapping()
    fun getVault(): GetVaultResponse {
        return vaultService.getVault()
    }

    @PostMapping("update")
    fun updateVault(@RequestBody request: UpdateVaultRequest) {
        vaultService.updateVault(request.addOrUpdate, request.delete)
    }
}