package com.alexkuzin.pandorica.controllers

import com.alexkuzin.pandorica.data.request.GetAccessTokenRequest
import com.alexkuzin.pandorica.data.request.SignInRequest
import com.alexkuzin.pandorica.data.request.SignUpRequest
import com.alexkuzin.pandorica.data.response.GetAccessTokenResponse
import com.alexkuzin.pandorica.data.response.SignUpResponse
import com.alexkuzin.pandorica.data.response.SignInResponse
import com.alexkuzin.pandorica.services.AuthorizationService
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("auth")
class AuthorizationController(
    val authorizationService: AuthorizationService
) {

    @PostMapping("/signUp")
    fun register(@RequestBody request: SignUpRequest): SignUpResponse {
        return authorizationService.signUp(request.login, request.password, request.encodedSecretKey, request.salt)
    }

    @PostMapping("/signIn")
    fun signIn(@RequestBody request: SignInRequest): SignInResponse {
        return authorizationService.signIn(request.login, request.password)
    }

    @PostMapping("/getAccessToken")
    fun getAccessToken(@RequestBody request: GetAccessTokenRequest): GetAccessTokenResponse {
        return authorizationService.getAccessToken(request.refreshToken)
    }
}