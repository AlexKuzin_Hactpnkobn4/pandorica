package com.alexkuzin.pandorica.data.enities

import com.amazonaws.services.dynamodbv2.model.AttributeValue
import com.amazonaws.services.dynamodbv2.model.GetItemResult

data class User(
    val login: String,
    val encodedPassword: String,
    val encodedSecretKey: String,
    val salt: String
){
    fun toDynamoDBItem(): Map<String, AttributeValue> {
        return mapOf(
            "login" to AttributeValue().withS(login),
            "encodedPassword" to AttributeValue().withS(encodedPassword),
            "encodedSecretKey" to AttributeValue().withS(encodedSecretKey),
            "salt" to AttributeValue().withS(salt),
        )
    }

    companion object{
        fun fromItem(getItemResult: GetItemResult): User {
            val login = getItemResult.item["login"]!!.s
            val encodedPassword = getItemResult.item["encodedPassword"]!!.s
            val encodedSecretKey = getItemResult.item["encodedSecretKey"]!!.s
            val salt = getItemResult.item["salt"]!!.s
            return User(
                login,
                encodedPassword,
                encodedSecretKey,
                salt
            )
        }
    }
}