package com.alexkuzin.pandorica.data.enities

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBIgnore
import com.amazonaws.services.dynamodbv2.model.AttributeValue
import com.amazonaws.services.dynamodbv2.model.GetItemResult

data class Vault(
    val login: String,
    val passwordEntries: Map<String, PasswordEntry> = emptyMap(),
    val timestamp: Long = System.currentTimeMillis()
) {
    fun toDynamoDBItem(): Map<String, AttributeValue> {
        return mapOf(
            "login" to AttributeValue().withS(login),
            "passwordEntries" to AttributeValue().withM(
                passwordEntries.map {
                    it.key to AttributeValue().withM(
                        mapOf(
                            "encodedLogin"      to AttributeValue().withS(it.value.encodedPassword),
                            "encodedPassword"   to AttributeValue().withS(it.value.encodedPassword),
                            "timestamp"         to AttributeValue().withN(it.value.timestamp.toString())
                        )
                    )
                }.toMap()
            ),
            "timestamp" to AttributeValue().withN(timestamp.toString())
        )
    }

    companion object{
        fun fromItem(getItemResult: GetItemResult): Vault {
            val login = getItemResult.item["login"]!!.s
            val passwordEntries = getItemResult.item["passwordEntries"]?.m?.
            map { passwordEntry ->
                passwordEntry.key to PasswordEntry(
                    name = passwordEntry.key,
                    encodedLogin =  passwordEntry.value.m["encodedLogin"]?.s,
                    encodedPassword = passwordEntry.value.m["encodedPassword"]!!.s ,
                    timestamp = passwordEntry.value.m["timestamp"]!!.n.toLong()
                )
            }?.toMap()
            val timestamp = getItemResult.item["timestamp"]!!.n.toLong()
            return Vault(
                login,
                passwordEntries ?: emptyMap(),
                timestamp
            )
        }
    }
}

data class PasswordEntry(
    @DynamoDBIgnore val name: String,
    val encodedLogin: String?,
    val encodedPassword: String,
    val timestamp: Long = System.currentTimeMillis()
)