package com.alexkuzin.pandorica.data.enities

import com.amazonaws.services.dynamodbv2.model.AttributeValue
import com.amazonaws.services.dynamodbv2.model.GetItemResult

data class Tokens(
    val login: String,
    val refreshTokenFamilies: Map<String, RefreshTokenFamily>
){
    fun toDynamoDBItem(): Map<String, AttributeValue> {
        return mapOf(
            "login" to AttributeValue().withS(login),
            "refreshTokenFamilies" to AttributeValue().withM(
                refreshTokenFamilies.map {  familyEntry ->
                    familyEntry.key to AttributeValue().withM(
                        mapOf(
                            "familyId" to AttributeValue().withS(familyEntry.value.familyId),
                            "freshToken" to AttributeValue().withS(familyEntry.value.freshToken),
                        )
                    )
                }.toMap()
            ),
        )
    }

    companion object{
        fun fromItem(getItemResult: GetItemResult): Tokens {
            val login = getItemResult.item["login"]!!.s
            val refreshTokenList = getItemResult.item["refreshTokenFamilies"]!!.m.map { familyEntry ->
                familyEntry.value.m["familyId"]!!.s to RefreshTokenFamily(
                    familyEntry.value.m["familyId"]!!.s,
                    familyEntry.value.m["freshToken"]!!.s,
                )
            }.toMap()
            return Tokens(
                login,
                refreshTokenList
            )
        }
    }
}

data class RefreshTokenFamily(
    val familyId: String,
    val freshToken: String,
)