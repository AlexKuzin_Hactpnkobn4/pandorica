package com.alexkuzin.pandorica.data.response

class SignUpResponse(
    val accessToken: String,
    val refreshToken: String,
    val encodedSecretKey: String,
    val salt: String
)