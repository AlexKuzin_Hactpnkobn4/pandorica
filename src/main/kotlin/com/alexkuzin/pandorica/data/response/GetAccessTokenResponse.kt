package com.alexkuzin.pandorica.data.response

class GetAccessTokenResponse(
    val accessToken: String,
    val refreshToken: String,
)