package com.alexkuzin.pandorica.data.response

class SignInResponse(
    val accessToken: String,
    val refreshToken: String,
    val encodedSecretKey: String,
    val salt: String
)