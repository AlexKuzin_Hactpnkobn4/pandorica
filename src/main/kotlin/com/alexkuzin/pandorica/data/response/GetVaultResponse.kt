package com.alexkuzin.pandorica.data.response

import com.alexkuzin.pandorica.data.enities.PasswordEntry

class GetVaultResponse(
    val passwordEntries: List<PasswordEntry>
)