package com.alexkuzin.pandorica.data.request

data class SignUpRequest(
    val login: String,
    val password: String,
    val encodedSecretKey: String,
    val salt: String
)