package com.alexkuzin.pandorica.data.request

import com.alexkuzin.pandorica.data.enities.PasswordEntry

class UpdateVaultRequest(
    val addOrUpdate: List<PasswordEntry>?,
    val delete: List<String>?
)