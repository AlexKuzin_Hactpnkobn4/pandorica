package com.alexkuzin.pandorica.data.request

data class SignInRequest(
    val login: String,
    val password: String
)