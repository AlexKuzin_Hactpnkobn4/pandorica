package com.alexkuzin.pandorica.data.request

data class GetAccessTokenRequest(
    val refreshToken: String
)