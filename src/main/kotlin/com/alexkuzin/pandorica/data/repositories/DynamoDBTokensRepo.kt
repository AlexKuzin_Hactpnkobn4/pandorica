package com.alexkuzin.pandorica.data.repositories

import com.alexkuzin.pandorica.data.enities.RefreshTokenFamily
import com.alexkuzin.pandorica.data.enities.Tokens
import com.alexkuzin.pandorica.utils.Constants
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB
import com.amazonaws.services.dynamodbv2.model.AttributeValue
import com.amazonaws.services.dynamodbv2.model.GetItemRequest
import com.amazonaws.services.dynamodbv2.model.UpdateItemRequest
import org.springframework.stereotype.Repository
import java.lang.RuntimeException

@Repository
class DynamoDBTokensRepo(val amazonDynamoDB: AmazonDynamoDB) {

    fun getTokensByLogin(login: String): Tokens {
        val item = amazonDynamoDB.getItem(
            GetItemRequest()
                .withTableName(Constants.DynamoDB.TABLE_NAME_TOKENS)
                .withKey(
                    mapOf(
                        "login" to AttributeValue().withS(login)
                    )
                )
        )
        if(item.item == null) throw RuntimeException("No user with login $login")
        return Tokens.fromItem(item)
    }

    fun saveOrUpdateRefreshTokenFamily(login: String, refreshTokenFamily: RefreshTokenFamily){
        amazonDynamoDB.updateItem(
            UpdateItemRequest()
                .withTableName(Constants.DynamoDB.TABLE_NAME_TOKENS)
                .withKey(mapOf("login" to AttributeValue().withS(login)))
                .withUpdateExpression(
                    """SET refreshTokenFamilies.#id = :family
                    """.trimMargin()
                )
                .withExpressionAttributeNames(mapOf("#id" to refreshTokenFamily.familyId))
                .withExpressionAttributeValues(
                    mapOf(
                        ":family" to AttributeValue().withM(
                            mapOf(
                                "familyId"       to AttributeValue().withS(refreshTokenFamily.familyId),
                                "freshToken"     to AttributeValue().withS(refreshTokenFamily.freshToken),
                            )
                        )
                    )
                )
        )
    }

    fun revokeRefreshTokenFamilyByFamilyId(login: String, familyId: String){
        amazonDynamoDB.updateItem(
            UpdateItemRequest()
                .withTableName(Constants.DynamoDB.TABLE_NAME_TOKENS)
                .withKey(mapOf("login" to AttributeValue().withS(login)))
                .withUpdateExpression(
                    """REMOVE refreshTokenFamilies.#id
                    """.trimMargin()
                )
                .withExpressionAttributeNames(mapOf("#id" to familyId))
        )
    }
}