package com.alexkuzin.pandorica.data.repositories

import com.alexkuzin.pandorica.data.enities.Tokens
import com.alexkuzin.pandorica.data.enities.User
import com.alexkuzin.pandorica.data.enities.Vault
import com.alexkuzin.pandorica.utils.Constants
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB
import com.amazonaws.services.dynamodbv2.model.Put
import com.amazonaws.services.dynamodbv2.model.ReturnConsumedCapacity
import com.amazonaws.services.dynamodbv2.model.TransactWriteItem
import com.amazonaws.services.dynamodbv2.model.TransactWriteItemsRequest
import org.springframework.stereotype.Repository

@Repository
class DynamoDBTransactionalRepo(
    val amazonDynamoDB: AmazonDynamoDB
) {

    fun createUser(user: User, tokens: Tokens, vault: Vault){
        val createUserPut = Put()
            .withTableName(Constants.DynamoDB.TABLE_NAME_USERS)
            .withItem(user.toDynamoDBItem())

        val createTokensPut = Put()
            .withTableName(Constants.DynamoDB.TABLE_NAME_TOKENS)
            .withItem(tokens.toDynamoDBItem())

        val createVaultPut = Put()
            .withTableName(Constants.DynamoDB.TABLE_NAME_VAULTS)
            .withItem(vault.toDynamoDBItem())

        val actions = listOf(
            TransactWriteItem().withPut(createUserPut),
            TransactWriteItem().withPut(createTokensPut),
            TransactWriteItem().withPut(createVaultPut),
        )

        val transactionRequest = TransactWriteItemsRequest()
            .withTransactItems(actions)
            .withReturnConsumedCapacity(ReturnConsumedCapacity.TOTAL)

        amazonDynamoDB.transactWriteItems(transactionRequest)
    }


}