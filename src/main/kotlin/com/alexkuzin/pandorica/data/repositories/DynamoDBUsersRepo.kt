package com.alexkuzin.pandorica.data.repositories

import com.alexkuzin.pandorica.config.exceptionHandling.exceptions.AuthenticationException
import com.alexkuzin.pandorica.data.enities.User
import com.alexkuzin.pandorica.utils.Constants
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB
import com.amazonaws.services.dynamodbv2.model.AttributeValue
import com.amazonaws.services.dynamodbv2.model.GetItemRequest
import org.springframework.stereotype.Repository

@Repository
class DynamoDBUsersRepo(val amazonDynamoDB: AmazonDynamoDB) {

    fun isUserExists(login: String): Boolean {
        val item = amazonDynamoDB.getItem(
            GetItemRequest()
                .withTableName(Constants.DynamoDB.TABLE_NAME_USERS)
                .withKey(
                    mapOf(
                        "login" to AttributeValue().withS(login)
                    )
                )
        )
        return item.item != null
    }

    fun getUserByLogin(login: String): User {
        val item = amazonDynamoDB.getItem(
            GetItemRequest()
                .withTableName(Constants.DynamoDB.TABLE_NAME_USERS)
                .withKey(
                    mapOf(
                        "login" to AttributeValue().withS(login)
                    )
                )
        )
        if(item.item == null) throw AuthenticationException()
        return User.fromItem(item)
    }

}