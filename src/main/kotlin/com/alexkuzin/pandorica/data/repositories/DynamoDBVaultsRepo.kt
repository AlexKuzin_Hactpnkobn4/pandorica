package com.alexkuzin.pandorica.data.repositories

import com.alexkuzin.pandorica.data.enities.PasswordEntry
import com.alexkuzin.pandorica.data.enities.Vault
import com.alexkuzin.pandorica.utils.Constants
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB
import com.amazonaws.services.dynamodbv2.model.*
import org.springframework.stereotype.Repository

@Repository
class DynamoDBVaultsRepo(val amazonDynamoDB: AmazonDynamoDB) {

    fun getVault(login: String): Vault {
        val itemResult = amazonDynamoDB.getItem(
            GetItemRequest()
                .withTableName(Constants.DynamoDB.TABLE_NAME_VAULTS)
                .withKey(mapOf("login" to AttributeValue().withS(login)))
        )
        return Vault.fromItem(itemResult)
    }


    fun updateEntries(
        login: String,
        entriesToAddOrUpdate: List<PasswordEntry>?,
        entriesToDelete: List<String>?
    ) {
        if(entriesToAddOrUpdate.isNullOrEmpty() && entriesToDelete.isNullOrEmpty()) {
            return
        }

        val setExpressionsList = ArrayList<String>()
        val removeExpressionsList = ArrayList<String>()
        val attributeNames = HashMap<String, String>()
        val attributeValues = HashMap<String, AttributeValue>()

        var i = 0
        entriesToAddOrUpdate?.forEach { entry ->
            val name = entry.name
            setExpressionsList.add("passwordEntries.#nameToUpdate${i} = :entry${i}")
            attributeNames["#nameToUpdate${i}"] = name
            attributeValues[":entry${i}"] = AttributeValue().withM(
                mapOf(
                    "encodedLogin" to AttributeValue().withS(entry.encodedLogin),
                    "encodedPassword" to AttributeValue().withS(entry.encodedPassword),
                    "timestamp" to AttributeValue().withN(entry.timestamp.toString()),
                )
            )
            i++
        }

        entriesToDelete?.forEach { name ->
            removeExpressionsList.add("passwordEntries.#nameToDelete${i}")
            attributeNames["#nameToDelete${i}"] = name
        }

        val updateExpression = StringBuilder()
        if(setExpressionsList.isNotEmpty()){
            updateExpression.append("SET ${setExpressionsList.joinToString(",")}  ")
        }
        if(removeExpressionsList.isNotEmpty()){
            updateExpression.append("REMOVE ${removeExpressionsList.joinToString(",")}  ")
        }

        var updateItemRequest = UpdateItemRequest()
            .withTableName(Constants.DynamoDB.TABLE_NAME_VAULTS)
            .withKey(mapOf("login" to AttributeValue().withS(login)))
            .withUpdateExpression(updateExpression.toString())
            .withExpressionAttributeNames(attributeNames)

        if(attributeValues.isNotEmpty()){
            updateItemRequest = updateItemRequest.withExpressionAttributeValues(attributeValues)
        }

        amazonDynamoDB.updateItem(updateItemRequest)
    }
}