package com.alexkuzin.pandorica.services

import com.alexkuzin.pandorica.config.exceptionHandling.exceptions.AuthenticationException
import com.alexkuzin.pandorica.data.enities.RefreshTokenFamily
import com.alexkuzin.pandorica.data.enities.Tokens
import com.alexkuzin.pandorica.data.enities.User
import com.alexkuzin.pandorica.data.enities.Vault
import com.alexkuzin.pandorica.data.repositories.DynamoDBTokensRepo
import com.alexkuzin.pandorica.data.repositories.DynamoDBTransactionalRepo
import com.alexkuzin.pandorica.data.repositories.DynamoDBUsersRepo
import com.alexkuzin.pandorica.data.response.GetAccessTokenResponse
import com.alexkuzin.pandorica.data.response.SignInResponse
import com.alexkuzin.pandorica.data.response.SignUpResponse
import com.alexkuzin.pandorica.security.JwtTokenService
import com.alexkuzin.pandorica.utils.Constants
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service
import java.util.*

@Service
class AuthorizationService(
    val usersRepo: DynamoDBUsersRepo,
    val tokensRepo: DynamoDBTokensRepo,
    val passwordEncoder: PasswordEncoder,
    val jwtTokenService: JwtTokenService,
    val dynamoDBTransactionalRepo: DynamoDBTransactionalRepo
) {

    fun signUp(
        login: String,
        password: String,
        encodedSecretKey: String,
        salt: String
    ): SignUpResponse {
        val isUserExists = usersRepo.isUserExists(login)
        if (isUserExists) throw AuthenticationException("User with login '$login' already exists!")
        val encodedPassword = passwordEncoder.encode(password)
        val familyId = UUID.randomUUID().toString()
        val accessToken = jwtTokenService.generateAccessToken(login, familyId)
        val refreshToken = jwtTokenService.generateRefreshToken(login, familyId)
        dynamoDBTransactionalRepo.createUser(
            user = User(login, encodedPassword, encodedSecretKey, salt),
            tokens = Tokens(
                login,
                mapOf(
                    familyId to RefreshTokenFamily(familyId, refreshToken)
                )
            ),
            vault = Vault(login)
        )
        return SignUpResponse(
            accessToken = accessToken,
            refreshToken = refreshToken,
            encodedSecretKey = encodedSecretKey,
            salt = salt
        )
    }

    fun signIn(login: String, password: String): SignInResponse {
        val user = usersRepo.getUserByLogin(login)
        val isPasswordCorrect = passwordEncoder.matches(password, user.encodedPassword)
        if (!isPasswordCorrect) throw AuthenticationException("Password is wrong!")
        val familyId = UUID.randomUUID().toString()
        val accessToken = jwtTokenService.generateAccessToken(login, familyId)
        val refreshToken = jwtTokenService.generateRefreshToken(login, familyId)
        tokensRepo.saveOrUpdateRefreshTokenFamily(login, RefreshTokenFamily(familyId, refreshToken))
        return SignInResponse(accessToken, refreshToken, user.encodedSecretKey, user.salt)
    }

    fun getAccessToken(refreshToken: String): GetAccessTokenResponse {
        val claims = jwtTokenService.getAllClaimsFromToken(refreshToken)

        val login = claims.subject
        val tokenType = claims[Constants.Jwt.TOKEN_CLAIM_NAME_TYPE].toString()
        val familyId = claims[Constants.Jwt.TOKEN_CLAIM_NAME_FAMILY_ID].toString()

        if (tokenType != Constants.Jwt.TOKEN_TYPE_REFRESH) {
            throw AuthenticationException()
        }

        val tokens = tokensRepo.getTokensByLogin(login)

        if (!tokens.refreshTokenFamilies.containsKey(familyId)) {
            throw AuthenticationException()
        }

        if (
            tokens.refreshTokenFamilies[familyId]?.freshToken != refreshToken ||
            jwtTokenService.isExpired(refreshToken)
        ) {
            tokensRepo.revokeRefreshTokenFamilyByFamilyId(login, familyId)
            throw AuthenticationException()
        }

        val newAccessToken = jwtTokenService.generateAccessToken(login, familyId)
        val newRefreshToken = jwtTokenService.generateRefreshToken(login, familyId)
        tokensRepo.saveOrUpdateRefreshTokenFamily(login, RefreshTokenFamily(familyId, newRefreshToken))
        return GetAccessTokenResponse(
            accessToken = newAccessToken,
            refreshToken = newRefreshToken
        )
    }
}