package com.alexkuzin.pandorica.services

import com.alexkuzin.pandorica.data.enities.PasswordEntry
import com.alexkuzin.pandorica.data.repositories.DynamoDBVaultsRepo
import com.alexkuzin.pandorica.data.response.GetVaultResponse
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Service

@Service
class VaultService(val vaultsRepo: DynamoDBVaultsRepo) {

    fun getVault(): GetVaultResponse {
        val login = SecurityContextHolder.getContext().authentication.name
        return GetVaultResponse(
            passwordEntries = vaultsRepo.getVault(login).passwordEntries.values.toList()
        )
    }

    fun updateVault(entriesToAddOrUpdate: List<PasswordEntry>?, entriesToDelete: List<String>?) {
        val login = SecurityContextHolder.getContext().authentication.name
        vaultsRepo.updateEntries(login, entriesToAddOrUpdate, entriesToDelete)
    }
}