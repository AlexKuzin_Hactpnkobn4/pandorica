package com.alexkuzin.pandorica.config.logging

import jakarta.annotation.PostConstruct
import jakarta.servlet.FilterChain
import jakarta.servlet.ServletRequest
import jakarta.servlet.ServletResponse
import jakarta.servlet.http.HttpServletRequest
import jakarta.servlet.http.HttpServletResponse
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Component
import org.springframework.web.filter.GenericFilterBean
import org.springframework.web.util.ContentCachingRequestWrapper
import org.springframework.web.util.ContentCachingResponseWrapper
import java.util.*

@Component
class LoggingFilter: GenericFilterBean(){

    @PostConstruct
    fun after(){
        log.info("info")
        log.debug("debug")
        log.trace("trace")
        log.error("error")
        log.warn("warn")
    }

    val prohibitedHeaders = listOf("Authorization")

    val log = LoggerFactory.getLogger(this.javaClass)
    override fun doFilter(request: ServletRequest, response: ServletResponse, filterChain: FilterChain) {
        val requestWrapper = if (request is ContentCachingRequestWrapper) request
            else ContentCachingRequestWrapper(request as HttpServletRequest)
        val responseWrapper = if (response is ContentCachingResponseWrapper) response
            else ContentCachingResponseWrapper (response as HttpServletResponse)

        filterChain.doFilter(requestWrapper, responseWrapper)

        val id = UUID.randomUUID().toString()

        logRequest(id, requestWrapper)
        logResponse(id, responseWrapper)
    }

    private fun logRequest(id: String, request: ContentCachingRequestWrapper){
        val elements = mutableMapOf(
            "path" to request.requestURI,
            "headers" to headersToString(request.headerNames, request::getHeader),
            "contentLength" to request.contentLength,
            "query" to request.queryString
        )
        val elementsString = elements
            .filter { it.value != null }
            .map { "${it.key}:${it.value}" }
            .joinToString(" ")

        log.info("Request - id:$id ${request.method} $elementsString")
    }

    private fun logResponse(id: String, response: ContentCachingResponseWrapper){
        val elements = mutableMapOf(
            "status" to HttpStatus.valueOf(response.status),
            "headers" to headersToString(response.headerNames, response::getHeader),
            "contentSize" to response.contentSize,
        )
        val elementsString = elements
            .filter { it.value != null }
            .map { "${it.key}:${it.value}" }
            .joinToString(" ")

        log.info("Response - id:$id $elementsString")
        response.copyBodyToResponse()
    }

    private fun headersToString(headerNames: Enumeration<String>, getHeaderValueFun: (String) -> String): String {
        val stringBuilder = StringBuilder("{")
        while(headerNames.hasMoreElements()){
            val headerName = headerNames.nextElement()
            if(!prohibitedHeaders.contains(headerName)){
                stringBuilder.append("\"${headerName}\":\"${getHeaderValueFun(headerName)}\"")
                if(headerNames.hasMoreElements()){
                    stringBuilder.append(", ")
                }
            }
        }
        return stringBuilder.append("}").toString()
    }

    private fun headersToString(headerNames: Collection<String>, getHeaderValueFun: (String) -> String): String {
        val stringBuilder = StringBuilder("{")
        headerNames.forEachIndexed{index, headerName ->
            if(!prohibitedHeaders.contains(headerName)){
                stringBuilder.append("\"${headerName}\":\"${getHeaderValueFun(headerName)}\"")
                if(index != headerNames.size - 1){
                    stringBuilder.append(", ")
                }
            }
        }
        return stringBuilder.append("}").toString()
    }
}