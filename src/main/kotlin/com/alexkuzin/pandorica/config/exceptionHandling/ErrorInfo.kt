package com.alexkuzin.pandorica.config.exceptionHandling

data class ErrorInfo(
    val message: String? = null
)