package com.alexkuzin.pandorica.config.exceptionHandling.exceptions

import org.springframework.http.HttpStatus

class AuthenticationException(errorMessage: String? = null) :
    BasicException(status = HttpStatus.UNAUTHORIZED, errorMessage)