package com.alexkuzin.pandorica.config.exceptionHandling.exceptions

import org.springframework.http.HttpStatus
import org.springframework.util.MultiValueMap

open class BasicException(
    val status: HttpStatus,
    val errorMessage: String? = null,
    val headers: MultiValueMap<String, String>? = null
) : RuntimeException()
