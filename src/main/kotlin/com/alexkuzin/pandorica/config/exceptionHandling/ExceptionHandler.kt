package com.alexkuzin.pandorica.config.exceptionHandling

import com.alexkuzin.pandorica.config.exceptionHandling.exceptions.BasicException
import jakarta.servlet.http.HttpServletRequest
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler

@ControllerAdvice
class ExceptionHandler : ResponseEntityExceptionHandler() {

    @ExceptionHandler(Exception::class)
    fun handle(req: HttpServletRequest, ex: Exception): ResponseEntity<ErrorInfo> {
        logger.error(ex.message, ex)
        return when(ex){
            is BasicException -> {
                ResponseEntity(
                    ex.errorMessage?.let { ErrorInfo(it) } ,
                    ex.headers,
                    ex.status
                )
            }
            else -> {
                ResponseEntity(
                    HttpStatus.INTERNAL_SERVER_ERROR
                )
            }
        }
    }
}