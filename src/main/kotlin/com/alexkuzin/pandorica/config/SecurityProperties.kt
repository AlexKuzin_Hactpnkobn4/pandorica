package com.alexkuzin.pandorica.config

import org.springframework.boot.context.properties.ConfigurationProperties

@ConfigurationProperties(prefix = "security")
class SecurityProperties {
    lateinit var secret: String
    lateinit var accessTokenDuration: String
    lateinit var refreshTokenDuration: String
}