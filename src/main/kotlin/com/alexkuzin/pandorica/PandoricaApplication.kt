package com.alexkuzin.pandorica

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.ConfigurationPropertiesScan
import org.springframework.boot.runApplication

@SpringBootApplication
@ConfigurationPropertiesScan
class PandoricaApplication

fun main(args: Array<String>) {
	runApplication<PandoricaApplication>(*args)
}
